#!/bin/sh

set -e

buildah login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

set -x

if [ ${CI_COMMIT_REF_NAME} = "main" ]; then
  tag="latest"
else
  tag="${CI_COMMIT_REF_NAME}.latest"
fi

for module in ${CONTAINER}; do
  buildah manifest create morello-${module}:${tag} \
    docker://${CI_REGISTRY_IMAGE}/morello-${module}:${CI_COMMIT_REF_NAME}.${CI_PIPELINE_ID}.amd64 \
    docker://${CI_REGISTRY_IMAGE}/morello-${module}:${CI_COMMIT_REF_NAME}.${CI_PIPELINE_ID}.arm64
  buildah manifest inspect morello-${module}:${tag}
  buildah manifest push --all --format v2s2 morello-${module}:${tag} docker://${CI_REGISTRY_IMAGE}/morello-${module}
done
