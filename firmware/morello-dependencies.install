#!/bin/sh

set -e

# Install repo
curl https://storage.googleapis.com/git-repo-downloads/repo > ${TOOLS_DIR}/bin/repo
chmod a+x ${TOOLS_DIR}/bin/repo

# Install CMake
curl --connect-timeout 5 --retry 5 --retry-delay 1 --create-dirs -fsSLo /tmp/cmake-Linux-x86_64.tar.gz \
  https://github.com/Kitware/CMake/releases/download/v3.21.2/cmake-3.21.2-Linux-x86_64.tar.gz
tar -xf /tmp/cmake-Linux-x86_64.tar.gz -C ${TOOLS_DIR} --strip-components=1

# Install fiptool
curl --connect-timeout 5 --retry 5 --retry-delay 1 --create-dirs -fsSLo /tmp/trusted-firmware-a-2.5.tar.gz \
  https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/snapshot/trusted-firmware-a-2.5.tar.gz
tar -xf /tmp/trusted-firmware-a-2.5.tar.gz -C /tmp
(cd /tmp/trusted-firmware-a-2.5; make -j$(nproc) fiptool; cp -a tools/fiptool/fiptool ${TOOLS_DIR}/bin)

# Install toolchains using fetch-tools script
BASE_TOOLS_DIR=$(dirname ${TOOLS_DIR})
git clone --depth 1 https://git.morello-project.org/morello/build-scripts.git ${BASE_TOOLS_DIR}/build-scripts
(cd ${BASE_TOOLS_DIR}; ./build-scripts/fetch-tools.sh -f none update; rm -rf build-scripts output)
