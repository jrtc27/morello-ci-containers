FROM docker.io/library/ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive
ENV TOOLS_DIR=/home/ci-runner/tools
ENV PATH=${TOOLS_DIR}/bin:${PATH}
# cmake and make are installed from toolchain-src/prebuilts/build-tools/
ENV PKG_DEPS="\
    bison \
    build-essential \
    ca-certificates \
    curl \
    flex \
    git \
    gnupg \
    jq \
    less \
    libtool \
    openssh-client \
    python3 \
    python3-distutils \
    python3-psutil \
    rsync \
    sudo \
    texinfo \
    xmlstarlet \
    xz-utils \
"

# Can be overriden at build time
ARG CI_RUNNER_PASSWORD=ci-runner

COPY morello-*.install /tmp/

RUN set -e ;\
    echo 'dash dash/sh boolean false' | debconf-set-selections ;\
    apt update -q=2 ;\
    apt full-upgrade -q=2 --yes ;\
    apt install -q=2 --yes --no-install-recommends ${PKG_DEPS} ;\
    if [ $(uname -m) == "x86_64" ]; then apt install -q=2 --yes --no-install-recommends crossbuild-essential-arm64; else apt install -q=2 --yes --no-install-recommends llvm-10 clang-10 libc++-10-dev lld-10 cmake; fi ;\
    # Set default shell to bash
    dpkg-reconfigure -p critical dash ;\
    # Set Python 3 as default
    update-alternatives --install /usr/bin/python python /usr/bin/python3 50 ;\
    # Setup ci-runner user
    useradd -m -s /bin/bash ci-runner ;\
    echo "ci-runner:$CI_RUNNER_PASSWORD" | chpasswd ;\
    echo 'ci-runner ALL = NOPASSWD: ALL' > /etc/sudoers.d/ci ;\
    chmod 0440 /etc/sudoers.d/ci ;\
    # Run shell script(s) to install files, toolchains, etc...
    mkdir -p ${TOOLS_DIR}/bin ;\
    bash -ex /tmp/morello-dependencies.install ;\
    bash -ex /tmp/morello-environment.install ;\
    # Fix permissions
    chown -R ci-runner:ci-runner ${TOOLS_DIR} ;\
    # Cleanup
    apt clean ;\
    rm -rf /var/lib/apt/lists/* /tmp/*

USER ci-runner

RUN set -e ;\
    # Set git default config
    git config --global user.email "ci@morello-project.org" ;\
    git config --global user.name "Morello CI" ;\
    git config --global color.ui "auto"

WORKDIR /home/ci-runner
CMD ["/bin/bash"]
